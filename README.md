## tmux-mem-cpu-load

[tmux-mem-cpu-load](https://github.com/thewtex/tmux-mem-cpu-load) packaging, which products the latest version of `tmux-mem-cpu-load` in `.deb` format. Since this is the latest version, you are running on the cutting edge if you choose to install.
